;; -*- coding:utf-8; mode:Scheme -*-

(module-name 'MyTest)
(module-compile-options main: #t)

;(require 'srfi-69) ;; Basic hash tables
;(require 'srfi-101) ;; Purely Functional Random-Access Pairs and Lists
(require 'srfi-1) ;;  List Library
(require 'srfi-2) ;;  AND-LET*: an AND with local bindings, a guarded LET* special form
(require 'srfi-8) ;;  receive: Binding to multiple values
(require 'srfi-10) ;; #, external form
(require 'srfi-13) ;; String Libraries
(require 'srfi-14) ;; Character-set Library
(require 'srfi-26) ;; Notation for Specializing Parameters without Currying
(require 'srfi-34) ;; Exception Handling for Programs
(require 'srfi-35) ;; Conditions
(require 'srfi-37) ;; args-fold: a program argument processor
(require 'srfi-41) ;; Streams
(require 'srfi-41-streams-derived);;
(require 'srfi-60) ;; Integers as Bits
(require 'srfi-64) ;; A Scheme API for test suites
(require 'srfi-95) ;; Sorting and Merging

;(require 'hash-table) ;;
(require 'list-lib) ;;
(require 'condition) ;;
(require 'conditions) ;;
(require 'testing);;

;(import kawa.lib.scheme.base)
(import kawa.lib.scheme.case-lambda)
(import kawa.lib.scheme.char)
(import kawa.lib.scheme.complex)
(import kawa.lib.scheme.cxr)
(import kawa.lib.scheme.eval)
(import kawa.lib.scheme.inexact)
(import kawa.lib.scheme.lazy)
(import kawa.lib.scheme.load)
(import kawa.lib.scheme.process-context)
(import kawa.lib.scheme.read)
(import kawa.lib.scheme.repl)
(import kawa.lib.scheme.time)
(import kawa.lib.scheme.write)
(import kawa.lib.scheme.r5rs)

;(import kawa.lib.rnrs.hashtables)
;(import kawa.lib.rnrs.lists)
(import kawa.lib.rnrs.arithmetic.bitwise)
(import kawa.lib.rnrs.programs)
(import kawa.lib.rnrs.sorting)
(import kawa.lib.rnrs.unicode)

;(import kawa.lib.kawa.base)
(import kawa.lib.kawa.arglist)
(import kawa.lib.kawa.expressions)
(import kawa.lib.kawa.hashtable)
(import kawa.lib.kawa.swing)
(import kawa.lib.kawa.regex)
(import kawa.lib.kawa.reflect)
(import kawa.lib.kawa.pprint)
(import kawa.lib.kawa.string-cursors)
(import kawa.lib.kawa.quaternions)

;(import gnu.kawa.slib.srfi69)
(import gnu.kawa.slib.srfi1)
(import gnu.kawa.slib.srfi2) 
(import gnu.kawa.slib.srfi13) 
(import gnu.kawa.slib.srfi14)
(import gnu.kawa.slib.srfi34)
(import gnu.kawa.slib.srfi37)
(import gnu.kawa.slib.srfi60)

(define (main)
    (display (iota 5 0 -0.5))
    (display "Hello"))
(main)
