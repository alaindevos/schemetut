;; -*- coding:utf-8; mode:Scheme -*-
(module-name MyTest)
(module-compile-options main: #t)

(define-simple-class 2d-vector ()
    (x type: double init-keyword: x:)
    (y type: double init-keyword: y:)
    ((*init* (x0 ::double) (y0 ::double))
        (set! x x0)
        (set! y y0))
    )

(define (main)
    (format #t "~a~%"
        (java.lang.Math:sqrt 9.0)) ; Calling java function
        
    (define v :: 2d-vector (2d-vector 1 2)) ; construct object
    (format #t "x: ~a~%" v:x))
(main)
