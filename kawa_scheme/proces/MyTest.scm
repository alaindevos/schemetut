;; -*- coding:utf-8; mode:Scheme -*-
(module-name MyTest)
(module-compile-options main: #t)
(define (main)
    (display (run-process "/usr/sbin/pkg info | /usr/bin/wc -l")))
(main)
