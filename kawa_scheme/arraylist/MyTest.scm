;; -*- coding:utf-8; mode:Scheme -*-
(module-name MyTest)
(module-compile-options main: #t)

(define (main)
    (define ar ::java.util.ArrayList[symbol] (java.util.ArrayList))
    (ar:add 'a)
    (ar:size)
    (ar:add 1))
(main)
