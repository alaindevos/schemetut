;; -*- coding:utf-8; mode:Scheme -*-
(module-name MyTest)
(module-compile-options main: #t)
(define (printc)
    (display "c"))
(require 'srfi-1) ;;  List Library
(define (main)
    (import MyTestA)
    (printa)
    (MyTestB:printb)
    (printc))
(main)
