;; -*- coding:utf-8; mode:Scheme -*-
(module-name MyTest)
(module-compile-options main: #t)

(define (fibo x)
    (if (< x 2) 
        1
        (+ (fibo (- x 2)) 
           (fibo (- x 1)))))
           
(define (main)
    (format #t "~a~%" (fibo 39)))

(main)
