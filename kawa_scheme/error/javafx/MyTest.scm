(require 'javafx-defs)
(javafx-application)
(javafx-scene
    title: "Hello Button"
    width: 600 height: 450
    (   Button
        text: "Click Me"
        layout-x: 25
        layout-y: 40
        on-action: (lambda (e) (format #t "Event: ~s~%" e))
        on-key-released: (lambda (e) (format #t "Event: ~s~%" e)))
    (   Button
        text: "Click Me Too"
        layout-x: 25
        layout-y: 70)
    (   Button
        text: "Click Me Three"
        layout-x: 25
        layout-y: 100))
