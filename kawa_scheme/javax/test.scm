;; -*- coding:utf-8; mode:Scheme -*-

(define-alias JLabel javax.swing.JLabel)
(define-alias JButton javax.swing.JButton)

(define-simple-class SimpleFrame (javax.swing.JFrame)
    (label ::JLabel allocation: 'instance  access: 'private 
        init: (JLabel "Kawa Swing Application"))
    (button allocation: 'instance  access: 'private 
        init: (JButton "Click Me"
                action-listener: (lambda (event)(on-click-btn))))
    ((*init*) ;; constructor
    (invoke-special javax.swing.JFrame (this) 
        '*init* "Hola Swing desde Kawa")
    (format #t "Initialising ...~%")
    (add label java.awt.BorderLayout:CENTER)
    (add button java.awt.BorderLayout:SOUTH)
    (setDefaultCloseOperation javax.swing.JFrame:EXIT_ON_CLOSE)
    (pack))
    ((on-click-btn) allocation: 'instance  access: 'protected
    (label:setText "You clicked!")))

(define (main)
  (let ((frame (SimpleFrame)))
    (frame:setVisible #t)))

(main)
