;Programs and data share the same syntax
(define W
  (lambda(x)
    (write x)
    (newline)
    )
  )
(W 'write)

;Make a list of data
(W  (quote (1 2 3)))
(W (quote ("Alain" "Eddy")))

;Abbrevation of quote
(W '(1 2 3))
(W '("Alain" "Eddy"))

;Make a symbol of an identifier
(W (quote mysymbol))

;Make a constant of a constant
(W (quote 2))
(W (quote "Alain"))

;Manipulate List
(W (car '(a b c)))
(W (cdr '(a b c)))

;Make list
(W (cons 'a '()))
(W (cons 'a '(b)))
(W (cons 'a (cons 'b '())))

;Pair
(W (cons 'a 'b))

;Let
(W (let ([x 2])
     (+ x 3)))
(W (let ([x 2]
         [y 3])
     (+ x y)))

;Lambda function
(W ((lambda (x)
      { + x x}) 2))

;Define
(W "Define")
(define doubleme
  (lambda (f x)
    { f x x}))
(W (doubleme + 2))
(define doubler
  (lambda [f]
    { lambda [x]
      {f x x }}))
(define doubleplus (doubler +))
(W (doubleplus 2))

;Conditional-if
(W "Condition-if")
(define abs2
  (lambda (n)
    {if [< n 0]
      (- 0 n)
      n
      }))
(W (abs2 -2))

;Conditional-cond
(W "Conditional-cond")
(define abs3
  (lambda (n)
    {cond
     ([= n 0] 0)
     ([< n 0] (- 0 n))
     ([> n 0] n)
     }))
(W (abs3 -2))

;Recursion
(W "recursion")
(define rec length2
  (lambda (lis)
    {if [null? lis]
    0
    (+ 1 (length2 (cdr lis)))
    }))
(W (length2 '(a b)))
(W "makelistcopy")
(define rec makelistcopy
  (lambda (lis)
    {if [null? lis]
    '()
    (cons (car lis) (makelistcopy (cdr lis)))
    }
    ))
(define mylist '(1 2))
(W mylist)
(W (makelistcopy '(a b)))
